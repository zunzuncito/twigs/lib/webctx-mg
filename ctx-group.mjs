
import path from 'path';

import FSUtil from 'zunzun/flyutil/fs.mjs'

import WebContext from './ctx.mjs'

export default class WebContextGroup {
  constructor(webctx_mg, route_path, dir_path, request_hook) {
    this.webctx_mg = webctx_mg;

    this.route_path = route_path;
    this.dir_path = dir_path;
    this.cfg = webctx_mg.cfg;

    this.contexts = []; // web contexts
    
    this.global_contexts = [];
    this.g_tpl_ctx = {}; // nunjucks template context

    this.next_mw_id = 0;
    this.middlewares = [];

    this.request_hook = request_hook || ((req, res, next) => {
      next()
    });
  }

  static async construct(webctx_mg, route_path, dir_path, request_hook) {
    try {
      const _this = new WebContextGroup(webctx_mg, route_path, dir_path, request_hook);

      await FSUtil.read_dir(dir_path, async (fpath, fname) => {
        try {
          if (fname === _this.cfg.context_files.main) {
            const rel_fpath = path.relative(dir_path, fpath);
            const ctx_route_path = _this.route_path.endsWith("/") || rel_fpath.length == 0 ?
              _this.route_path+rel_fpath :
              _this.route_path+"/"+rel_fpath;
            let new_route_context = new WebContext(_this, ctx_route_path, fpath)
            await new_route_context.read_directory();
            _this.contexts.push(new_route_context);
          }
        } catch (e) {
          console.error(e.stack);
        }
      });
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  async add_context(ctx_route_path, ctx_dir_path) {
    try {
      ctx_route_path = this.route_path.endsWith("/") ?
        this.route_path+ctx_route_path :
        this.route_path+"/"+ctx_route_path;
      let new_route_context = new WebContext(this, ctx_route_path, ctx_dir_path)
      await new_route_context.read_directory();

      const flyauth = this.flyauth
      for (let dmw of flyauth.default_middlewares) {
        new_route_context.add_middleware(dmw);
      }
      if (
        new_route_context.lctx &&
        new_route_context.lctx.flyauth &&
        new_route_context.lctx.flyauth.authorize
      ) {
        console.debug("ADDING AUTHORIZE MIDDLEWARE");
        new_route_context.add_middleware(flyauth.authorize_mwfn);
      } else {
        console.debug("ADDING FORWARD CREDENTIALS MIDDLEWARE");
        new_route_context.add_middleware(flyauth.forward_credentials_mwfn);
      }

      this.contexts.push(new_route_context);
      return new_route_context;
    } catch (e) {
      console.error(e.stack);
    }
  }

  async add_middleware(mwfn) {
    const mw = {
      id: this.next_mw_id,
      fn: mwfn
    };
    this.middlewares.push(mw);

    for (let context of this.contexts) {
      context.add_middleware(mw.fn, mw.id);
      await context.serve(true);
    }

    this.next_mw_id++;
    return mw.id;
  }

  async remove_middleware(mwid) {
    for (let i = this.middlewares.length-1; i >= 0; i--) {
      if (this.middlewares[i].id == mwid) {
        this.middlewares.splice(i, 1);
      }
    }

    for (let context of this.contexts) {
      context.remove_middleware(mwid);
      await context.serve(true);
    }

    return mw.id;
  }

  async serve() {
    try {
      for (let context of this.contexts) {
        await context.serve();
      }
    } catch (e) {
      console.error(e.stack);
    }
  }
  
  get_context(route_path) {
    for (let context of this.contexts) {
      if (route_path == context.route_path) {
        return context;
      }
    }
  }
}
