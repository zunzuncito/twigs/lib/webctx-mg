# Web Context Manager `webctx-mg`

## Starting
Construct `WebContextManager` object:
```
    webctx_mg = new WebContextManager(service, cfg);
```
Argument `service` should be the instance of a service calling the constructor


## Serving group
To start serving a context group call:

```
    await webctx_mg.serve_context_group(grp_route_path, grp_dir_path, [request_hook]);
```
specifying the root route path for the group and a full path to the top directory. Optionally use the third argument `request_hook` which is supposed to be a middleware function accepting `req, res, next` parameters. It will be called with every request.

Within the `grp_dir_path` directory every `.webcontext.yaml` file identifies the root path of an individual context and is used to describe it. `.webignore` can be used to specify ignored files.

## Updating group
To map new files to serve and reload configurations update context group calling:

```
    await webctx_mg.update_context_group(grp_route_path)
```

Use `grp_route_path` value you passed to `serve_context_group` method to update the same group.
