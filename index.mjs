

import ContextGroup from './ctx-group.mjs'

export default class WebContextManager {
  constructor(flyweb, cfg) {
    this.flyweb = flyweb;
    this.cfg = cfg;
    this.ctx_groups = [];
    this.c_etag = 0;
  }

  async serve_context_group(grp_route_path, grp_dir_path, request_hook) {
    try {
      console.log(`Serve context group: route: "${grp_route_path}"; directory: "${grp_dir_path}"`);
      const ctx_grp = await ContextGroup.construct(this, grp_route_path, grp_dir_path, request_hook);
      this.ctx_groups.push(ctx_grp);
      return ctx_grp;
    } catch (e) {
      console.error(e.stack);
    }
  }

  async update_context_group(grp_route_path) {
    try {
      for (let ctx_grp of this.ctx_groups) {
        if (ctx_grp.route_path === grp_route_path) {
          for (let ctx of ctx_grp.contexts) {
            await ctx.read_directory();
            await ctx.serve();
          }
        }
      }
      
    } catch (e) {
      console.error(e.stack);
    }
  }

  get_ctx_group(route_path) {
    for (let ctx_group of this.ctx_groups) {
      if (route_path === ctx_group.route_path) {
        return ctx_group;
      }
    }
  }

  get_context(route_path) {
    if (route_path.endsWith("index.html")) route_path = route_path.slice(0, -10);
    for (let ctx_group of this.ctx_groups) {
      if (route_path.startsWith(ctx_group.route_path)) {
        let ectx = ctx_group.get_context(route_path);
        if (ectx) return ectx;
      }
    }
  }


  async add_middleware(mwfn) {
    for (let ctx_group of this.ctx_groups) {
      await ctx_group.add_middleware(mwfn);
    }
  }
}
