
import crypto from 'crypto'

import fs from 'fs'
import path from 'path'

import zlib from 'zlib'

import nunjucks from 'nunjucks'

import YAML from 'yaml'
import url from 'url'

import {fileTypeFromStream} from 'file-type'

import ObjectUtil from 'zunzun/flyutil/object.mjs'
import ImportUtil from 'zunzun/flymodule/index.mjs'
import FSUtil from 'zunzun/flyutil/fs.mjs'
import HTTPUtil from 'zunzun/flyutil/http.mjs'


function escape_regex(string) {
    return string.replace(/[/\-\\^$*+?.()|[\]{}]/g, '\\$&');
}

export default class WebContext {

  constructor(ctx_group, route_path, dir_path) {
    this.ctx_group = ctx_group;
    this.cwd = ctx_group.webctx_mg.cfg.cwd;

    this.dir_path = dir_path;

    this.cfg = ctx_group.cfg;
    this.flyweb = ctx_group.webctx_mg.flyweb;

    this.request_hook = ctx_group.request_hook;

    this.ectx = {};

    this.public_files = [];

    const _this = this;

    this.web_ctx_mw = (req, res, next) => {
      if (!req.web_ctx) req.web_ctx = {};
      req.web_ctx[ctx_group.route_path] = _this;
      next();
    }

    this.ctx_file_path = path.resolve(dir_path, this.cfg.context_files.main);

    this.lang_dir = path.resolve(dir_path, ".lang");
    
    this.reload_lctx();

    this.route_path = ( this.lctx && this.lctx.path ) ? (
      this.lctx.path.string || new RegExp(this.lctx.path.regexp)
    ) : route_path;
    if (
      !(this.route_path instanceof RegExp) &&
      !this.route_path.endsWith("/")
    ) {
      this.route_path = this.route_path+"/";
    }

//    this.route_path = !(this.route_path instanceof RegExp) ? encodeURI(this.route_path) : this.route_path;

    this.mw_file_path = path.resolve(dir_path, this.cfg.middleware_file);

    this.next_mw_id = 0;
    this.middlewares = [];
    this.mwfns = [];

    this.g_mws = {
      first: [],
      render: [],
      last: []
    };

    this.fhttp = this.flyweb.http;

    let NJKLoader = {
      async: true,
      getSource: function(name, callback) {
        (async function() {
          try {
            const tmpl_path = name.charAt(0) === '/' ? 
              path.resolve(ctx_group.dir_path, name.substring(1)) :
              path.resolve(dir_path, name); 
            let tmpl_src = fs.readFileSync(tmpl_path, "utf8");
            callback(false, {
              src: tmpl_src,
              noCache: true
            });
          } catch (e) {
            console.error(e.stack);
          }
        })();
      }
    };

    this.nunjucks_env = new nunjucks.Environment(NJKLoader, { noCache: true });

  }

  reload_lctx(req) {
    try {
      this.lctx = YAML.parse(fs.readFileSync(this.ctx_file_path, 'utf8')) || {};

      if (req) { // TODO: move this shit to HTTP util
        const cookies = {};
        const cstrs = req.headers.cookie ? req.headers.cookie.split(";") : [];
        for (let cstr of cstrs) {
          const key_val = cstr.trim().split("=");
          cookies[key_val[0]] = key_val[1];
        }

        req.cookies = cookies;
      }

      let req_lang = req && req.params ? req.params["lang"] : undefined;

      const cur_lang = this.lctx.cur_lang = 
        typeof req_lang === "string" && 
        this.flyweb.cfg.available_languages.includes(req_lang) ?
          req_lang : (
            (req && req.cookies && req.cookies["language"]) ?
              req.cookies["language"] :
              this.flyweb.cfg.default_language
      );

      const cur_lang_json_path = path.resolve(this.lang_dir, cur_lang+".json");
      if (fs.existsSync(cur_lang_json_path)) {
        try {
          const cur_lang_json = fs.readFileSync(cur_lang_json_path, "utf8");
          this.lctx.lang_encoded = encodeURI(cur_lang_json);
          this.lctx.lang = JSON.parse(cur_lang_json);
        } catch (e) {
          console.log(`Error loading lang file: ${cur_lang_json_path}`);
          console.error(e.stack);
        }
      }

      if (this.lctx && this.lctx.global) {
        this.ctx_group.g_tpl_ctx = ObjectUtil.force_add(this.ctx_group.g_tpl_ctx, this.lctx);
        if (!this.ctx_group.global_contexts.includes(this)) {
          this.ctx_group.global_contexts.push(this);
        }
      } else {
        for (let gctx of this.ctx_group.global_contexts) {
          gctx.reload_lctx(req);
        }
      }

    } catch (e) {
      console.log(`Error loading context file: ${this.ctx_file_path}`);
      console.error(e.stack);
    }
  }

  async read_directory() {
    try {
      const _this = this;

      const ignore = [];

      const not_wctx_root = (dir_path) => { 
        if (fs.existsSync(
          path.resolve(dir_path, _this.cfg.context_files.main)
        )) {
          return false
        } else {
          return true
        }
      }

      FSUtil.read_dir_async(this.dir_path, (fpath, fname) => {
        if (fname == _this.cfg.context_files.ignore) {
          const ign_file = fs.readFileSync(path.resolve(fpath, fname), 'utf8');
          const nign = {
            dir_path: path.relative(_this.dir_path, fpath),
            expr: ign_file.split(/\r?\n/)
          }
          for (let e = nign.expr.length-1; e > -1; e--) {
            let expr = nign.expr[e];
            if (expr.length == 0) {
              nign.expr.splice(e, 1);
            } else {
              nign.expr[e] = new RegExp(expr);
            }
          }

          ignore.push(nign)
        }
      }, not_wctx_root);

      const ignored = (dir_path, file_path, fname) => {
        if (
          fname === _this.cfg.context_files.main ||
          fname === _this.cfg.context_files.ignore
        ) {
          return true;
        }

        for (let ign of ignore) {
          if (dir_path.startsWith(ign.dir_path)) {
            for (let regex of ign.expr) {
              const ign_matches = file_path.match(regex);
              if (ign_matches && ign_matches.length > 0) {
                return true;
              }
            }
          }
        }

        return false;
      }

      const removed_files = [];
      for (let epubf of _this.public_files) {
        if (!fs.existsSync(epubf.full_path)) removed_files.push(epubf);
      }

      for (let rmpubf of removed_files) {
        for (let epubf of _this.public_files) {
          if (rmpubf.full_path == epubf.full_path) {
            if (epubf.redirects) {
              for (let redir of epubf.redirects) {
                this.fhttp.clear(redir);
              }
            }
            this.fhttp.clear(epubf.route_id);
            _this.public_files.splice(_this.public_files.indexOf(epubf), 1);
            break;
          }
        }
      }

      await FSUtil.read_dir(this.dir_path, async (fpath, fname) => {
        try {
          const full_path = path.resolve(fpath, fname);
          const full_path_uri = path.resolve(fpath, fname);
          const dir_path = path.relative(_this.dir_path, path.resolve(fpath));
          const file_path = path.relative(_this.dir_path, full_path_uri);
          if (!ignored(dir_path, file_path, fname)) {
            let exists = false;
            for (let epubf of _this.public_files) {
              if (epubf.full_path === full_path) {
                exists = true;
                break;
              }
            }
            if (exists) return;

            const stream = fs.createReadStream(full_path);
            let file_type = await fileTypeFromStream(stream);
            if (!file_type || fname.endsWith(".svg")) {
              if (fname.endsWith(".html")) {
                file_type = { ext: 'html', mime: 'text/html' }
              } else if (fname.endsWith(".css")) {
                file_type = { ext: 'css', mime: 'text/css', encode: true }
              } else if (fname.endsWith(".js.map")) {
                file_type = { ext: 'js.map', mime: 'application/json', public: true }
              } else if (fname.endsWith(".js")) {
                file_type = { ext: 'js', mime: 'text/javascript', encode: true }
              } else if (fname.endsWith(".mp4")) {
                file_type = { ext: 'mp4', mime: 'video/mp4', stream: true }
              } else if (fname.endsWith(".mp3")) {
                file_type = { ext: 'mp3', mime: 'video/mpeg', stream: true }
              } else if (fname.endsWith(".png")) {
                file_type = { ext: 'png', mime: 'image/png', stream: true }
              } else if (fname.endsWith(".jpg")) {
                file_type = { ext: 'jpg', mime: 'image/jpeg', stream: true }
              } else if (fname.endsWith(".webp")) {
                file_type = { ext: 'webp', mime: 'image/webp', stream: true }
              } else if (fname.endsWith(".svg")) {
                file_type = { ext: 'svg', mime: 'image/svg+xml', stream: true }
              } else if (fname.endsWith(".bson")) {
                file_type = { ext: 'bson', mime: 'application/bson', stream: true }
              }  else if (fname.endsWith(".bin")) {
                file_type = { ext: 'bin', mime: 'application/octet-stream', stream: true }
              } else {
                file_type = { ext: '', mime: 'text/plain', encode: true }
              }
            } else {
              file_type.stream = true;
            }
            _this.public_files.push({
              name: fname,
              rel_path: file_path,
              full_path: full_path,
              type: file_type
            });
          }
        } catch (e) {
          console.error(e.stack);
        }
      }, not_wctx_root);

    } catch (e) {
      console.error(e.stack);
    }
  }


  add_middleware(mwfn, mwid) {
    const mw = {
      id: typeof mwid == 'number' ? mwid : this.next_mw_id,
      fn: mwfn
    }

    this.middlewares.push(mw);

    this.reassign_mwfns();

    this.next_mw_id++;
    return mw.id;
  }

  remove_middleware(mwid) {
    for (let i = this.middlewares.length-1; i >= 0; i--) {
      if (this.middlewares[i].id == mwid) {
        this.middlewares.splice(i, 1);
      }
    }

    this.reassign_mwfns();
  }

  reassign_mwfns() {
    this.mwfns = [];
    for (let mw of this.middlewares) {
      this.mwfns.push(mw.fn);
    }
  }

  async load_middleware() {
    try {
      const nocache = true; // TODO only in dev mode
      if (fs.existsSync(this.mw_file_path)) {
        return (await ImportUtil.load(path.dirname(this.mw_file_path), path.basename(this.mw_file_path), nocache)).default;
      }

    } catch (e) {
      console.error(e.stack);
      process.exit();
    }
  }

  async serve(force_update) {
    try {
      const request_hook = this.request_hook;


      const cmw = [ ...this.mwfns ];



      console.debug(`\x1b[47m\x1b[30m> SERVE CONTEXT \x1b[0m\x1b[1m ${this.ctx_file_path}\x1b[0m`);
      const _this = this;


      for (let pub_file of this.public_files) {
        if (!pub_file.served || force_update) {
          if (pub_file.served && force_update) {
            if (pub_file.redirects) {
              for (let redir of pub_file.redirects) {
                this.fhttp.clear(redir);
              }
            }
            this.fhttp.clear(pub_file.route_id);
//            _this.public_files.splice(_this.public_files.indexOf(pub_file), 1);
          }
          const web_path = pub_file.rel_path;
          const web_dir_path = path.relative(this.dir_path, path.dirname(pub_file.full_path));

          let route_path = this.route_path;
          if (this.route_path instanceof RegExp) {
            const sub_path_str = web_path.substring(web_dir_path.split("/")[0].length);
            route_path = new RegExp(this.route_path.source+"\/"+escape_regex(sub_path_str)+`($|\\?)`);
          } else {
            route_path += route_path.endsWith("/") ? web_path : "/"+web_path;
          }

          if (pub_file.name.endsWith(".html")) {

            const redirect_paths = []
            if (pub_file.name === "index.html") {
              if (web_dir_path.length > 0) {
                redirect_paths.push(this.route_path+web_dir_path);
                redirect_paths.push(this.route_path+web_dir_path+'/');
              } else if (!(this.route_path instanceof RegExp)) {
                if (this.route_path.length > 1) {
                  redirect_paths.push(this.route_path);
                  redirect_paths.push(this.route_path.slice(0, -1));
                } else {
                  redirect_paths.push(this.route_path);
                  redirect_paths.push(route_path);
                  route_path = this.route_path.slice(0, -1);
                }
              }

            }
            if (route_path.length == 0) route_path = '/';

            const endpoint_route = route_path instanceof RegExp ? route_path : encodeURI(route_path);

            console.debug(`\x1b[47m\x1b[30m> > SERVE PAGE \x1b[0m\x1b[1m ${endpoint_route}\x1b[0m`);
            pub_file.route_id = this.fhttp.get(endpoint_route, _this.web_ctx_mw, ...cmw, request_hook, async (req, res) => {
              try {

                const lang_param = req.params["lang"];

                _this.reload_lctx(req);
                let ctx = ObjectUtil.force_add(_this.lctx, _this.ectx );
                ctx = ObjectUtil.force_add(_this.ctx_group.g_tpl_ctx, ctx);
                ctx.cur_page_path = req.url.includes("?") ? req.url.split("?")[0] : req.url;
                ctx.available_languages = _this.flyweb.cfg.available_languages;

                const nonce = crypto.randomBytes(16).toString('base64');
                const header_obj = {
                  'Cache-Control': 'must-revalidate',
                  'Vary': 'User-Agent',
                  ..._this.fhttp.get_secure_headers(nonce)
                }


                if (lang_param && _this.flyweb.cfg.available_languages.includes(lang_param)) {
                  header_obj['Set-Cookie'] = [ `language=${lang_param}; Path=/; SameSite=Strict; Secure; HttpOnly` ]
                  ctx.current_language = lang_param;
                } else if (!(req && req.cookies && req.cookies["language"])) {
                  const preferred_langs = req.headers['accept-language'] ? req.headers['accept-language'].split(',') : [];
                  let set_lang = _this.flyweb.cfg.default_language;
                  for (const pref_lang of preferred_langs) {
                    const lang_tag = pref_lang.split(';')[0].trim().split('-')[0]
                    if (_this.flyweb.cfg.available_languages && _this.flyweb.cfg.available_languages.includes(lang_tag)) {
                      set_lang = lang_tag;
                      break;
                    }
                  }
                  header_obj['Set-Cookie'] = [ `language=${set_lang}; Path=/; SameSite=Strict; Secure; HttpOnly` ];
                  ctx.current_language = set_lang;
                } else {
                  ctx.current_language = req.cookies["language"];
                }


                let raw_html = fs.readFileSync(pub_file.full_path, 'utf8');
                

                for (let gctx of _this.ctx_group.global_contexts) {
                  const gmw_instance = await gctx.load_middleware();
                  if (gmw_instance) {
                    if (gmw_instance.render) raw_html = await gmw_instance.render(req, res, raw_html, ctx, _this.flyweb.sm);
                  }
                }



                const mw_instance = await _this.load_middleware();
                if (mw_instance) {
                  if (mw_instance.first) cmw.unshift(mw_instance.first);
                  if (mw_instance.last) cmw.push(mw_instance.last);
                }


                if (mw_instance && mw_instance.render) {
                  raw_html = await mw_instance.render(req, res, raw_html, ctx, _this.flyweb.sm);
                  if (!raw_html) return;
                }

                ctx.csp_nonce = nonce;
                let rdy_html = _this.nunjucks_env.renderString(raw_html, ctx);

                if (mw_instance && mw_instance.rendered) {
                  rdy_html = await mw_instance.rendered(req, res, rdy_htm, _this.flyweb.sml);
                  if (!rdy_html) return;
                }

                const content_hash = crypto.createHash('md5');
                content_hash.update(rdy_html);
                const content_etag = content_hash.digest('hex');

                header_obj['ETag'] = content_etag;

                if (req.headers['if-none-match'] === content_etag) {
                  res.writeHead(304, header_obj);
                  res.end();
                } else {
                  header_obj['Content-Type'] = 'text/html';
                  res.writeHead(200, header_obj);
                  res.write(rdy_html);
                  res.end();
                }

              } catch (e) {
                console.error(e.stack);
                res.writeHead(500, {
                  "Content-Type": "text/plain",
                  ..._this.fhttp.secure_headers
                });
                res.end("Internal Server Error");
              }

            });


            if (redirect_paths.length > 0) {
              pub_file.redirects = [];
              console.debug(`\x1b[47m\x1b[30m> > SERVE REDIRECT \x1b[0m\x1b[1m ${redirect_paths.join(" ")}\x1b[0m`);
              pub_file.redirects.push(this.fhttp.get(redirect_paths, _this.web_ctx_mw, ...cmw, request_hook, (req, res) => {
                let redirect_dst = route_path;
                if (req.method === 'GET') {
                  const parsed_url = url.parse(req.url, true);
                  redirect_dst =  redirect_dst+(parsed_url.search || "");
                }

                res.writeHead(302, {
                  'Location': redirect_dst,
                  ..._this.fhttp.secure_headers
                });
                res.end();
              }));
            }
          } else {
            const file_type = pub_file.type;

            if (file_type.public) {
              const endpoint_route = route_path instanceof RegExp ? route_path : encodeURI(route_path);
              console.debug(`\x1b[47m\x1b[30m> > > SERVE FILE  ${file_type.mime} \x1b[0m\x1b[1m ${endpoint_route}\x1b[0m`);

              pub_file.route_id = this.fhttp.get(endpoint_route, _this.web_ctx_mw, request_hook, async (req, res) => {
                try {
                  let mime_type = pub_file.type.mime;

                  const file_contents = fs.readFileSync(pub_file.full_path, 'utf8');

                  const compressed = await new Promise((fulfil) => {
                    if (req.headers['accept-encoding'].includes("br")) {
                      zlib.brotliCompress(file_contents, {
                        params: {
                          [zlib.constants.BROTLI_PARAM_MODE]: zlib.constants.BROTLI_MODE_TEXT,
                          [zlib.constants.BROTLI_PARAM_QUALITY]: zlib.constants.BROTLI_MIN_QUALITY // minimum quality since it take really long
                        }
                      }, (err, data) => {
                        if (err) {
                          console.error("brotliCompress: An error occurred:", err);
                        } else {
                          fulfil(data);
                        }
                      })
                    } else {
                      zlib.gzip(file_contents, function (_, result) {
                        fulfil(result);
                      });
                    }
                  });


                  const content_hash = crypto.createHash('md5');
                  content_hash.update(compressed);
                  const content_etag = content_hash.digest('hex');

                  const resp_head = {
                    'Cache-Control': 'must-revalidate',
                    'Content-Encoding': req.headers['accept-encoding'].includes("br") ? 'br' : 'gzip',
                    'ETag': content_etag,
                    'Vary': 'User-Agent',
                    ..._this.fhttp.secure_headers
                  }

                  if (req.headers['if-none-match'] === content_etag) {
                    res.writeHead(304, resp_head);
                    res.end();
                  } else {
                    resp_head['Content-Type'] = mime_type;
                    res.writeHead(200, resp_head);
                    res.write(compressed);
                    res.end();
                  }
                } catch (e) {
                  console.error(e.stack);
                }
              });

            } else if (file_type.stream) {
              const endpoint_route = route_path instanceof RegExp ? route_path : encodeURI(route_path);
              
              console.debug(`\x1b[47m\x1b[30m> > > SERVE FILE  ${file_type.mime} \x1b[0m\x1b[1m ${endpoint_route}\x1b[0m`);
              pub_file.route_id = this.fhttp.get(endpoint_route, _this.web_ctx_mw, ...cmw, request_hook, async (req, res) => {
                try {
                  if (!pub_file.e_tag) {
                    pub_file.e_tag = await new Promise((fulfil) => {
                      const content_hash = crypto.createHash('md5');

                      const stream = fs.createReadStream(pub_file.full_path);
                      stream.on('data', (data) => {
                        content_hash.update(data);
                      });

                      stream.on('end', () => {
                        fulfil(content_hash.digest('hex'));
                      });

                      stream.on('error', (err) => {
                        console.error(err);
                        fulfil();
                      });
                    });
                  }

                  const resp_head = {
                    'Cache-Control': 'must-revalidate',
                    'ETag': pub_file.e_tag,
                    'Vary': 'User-Agent',
                    ..._this.fhttp.secure_headers
                  }

                  if (req.headers['if-none-match'] === pub_file.e_tag) {
                    res.writeHead(304, resp_head);
                    res.end();
                  } else {
                    console.log("file stream", req.url);
                    HTTPUtil.file_stream(req, res, {
                      path: pub_file.full_path,
                      mime: file_type.mime,
                      headers: resp_head
                    });
                  }
       
                } catch (e) {
                  console.error(e.stack)
                }
              });
            } else {
              const endpoint_route = route_path instanceof RegExp ? route_path : encodeURI(route_path);

              console.debug(`\x1b[47m\x1b[30m> > > SERVE FILE \x1b[0m\x1b[1m ${route_path}\x1b[0m`);
              pub_file.route_id = this.fhttp.get(endpoint_route, _this.web_ctx_mw, ...cmw, request_hook, async (req, res) => {
                try {
                  let mime_type = pub_file.type.mime;

                  const file_contents = fs.readFileSync(pub_file.full_path, 'utf8');

                  const compressed = await new Promise((fulfil) => {
                    if (req.headers['accept-encoding'].includes("br")) {
                      zlib.brotliCompress(file_contents, {
                        params: {
                          [zlib.constants.BROTLI_PARAM_MODE]: zlib.constants.BROTLI_MODE_TEXT,
                          [zlib.constants.BROTLI_PARAM_QUALITY]: zlib.constants.BROTLI_MIN_QUALITY // minimum quality since it take really long
                        }
                      }, (err, data) => {
                        if (err) {
                          console.error("brotliCompress: An error occurred:", err);
                        } else {
                          fulfil(data);
                        }
                      })
                    } else {
                      zlib.gzip(file_contents, function (_, result) {
                        fulfil(result);
                      });
                    }
                  });


                  const content_hash = crypto.createHash('md5');
                  content_hash.update(compressed);
                  const content_etag = content_hash.digest('hex');

                  const resp_head = {
                    'Cache-Control': 'must-revalidate',
                    'Content-Encoding': req.headers['accept-encoding'].includes("br") ? 'br' : 'gzip',
                    'ETag': content_etag,
                    'Vary': 'User-Agent',
                    ..._this.fhttp.secure_headers
                  }

                  if (req.headers['if-none-match'] === content_etag) {
                    res.writeHead(304, resp_head);
                    res.end();
                  } else {
                    resp_head['Content-Type'] = mime_type;
                    res.writeHead(200, resp_head);
                    res.write(compressed);
                    res.end();
                  }
                } catch (e) {
                  console.error(e.stack);
                }
              });
            }

          }
        }
        pub_file.served = true;
      }
    } catch (e) {
      console.error(e.stack);
    }
  }
}
